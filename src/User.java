import java.util.Scanner;

public class User {
    private final Scanner inputScanner;

    public User() {
        inputScanner = new Scanner(System.in);
    }

    public RockPaperScissors.Move getMove() {
        System.out.print("Камень, ножницы или бумага? ");
        String userInput = inputScanner.nextLine();
        userInput = userInput.toUpperCase();
        char firstLetter = userInput.charAt(0);
        if (firstLetter == 'К' || firstLetter == 'Н' || firstLetter == 'Б') {
            switch (firstLetter) {
                case 'К':
                    return RockPaperScissors.Move.ROCK;
                case 'Н':
                    return RockPaperScissors.Move.PAPER;
                case 'Б':
                    return RockPaperScissors.Move.SCISSORS;
            }
        }
        return getMove();
    }

    public boolean playAgain() {
        System.out.print("Хотите сыграть еще раз? ");
        String userInput = inputScanner.nextLine();
        userInput = userInput.toUpperCase();
        return userInput.charAt(0) == 'Д';

    }
}
